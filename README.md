## Real Time Random Agent

A generic random agent for any real time strategy game.

Performs random actions at a random interval.
The intervals boundaries should be adjusted to the specific game for reasonable results.
